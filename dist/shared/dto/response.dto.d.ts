export declare class RequestDto {
    message: string;
    status: string;
    data: object;
}
export declare class ResponseDto {
    message: string;
    status: string;
    data: object;
    token?: string;
}
