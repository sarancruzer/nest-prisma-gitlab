"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.JwtPayloadService = void 0;
const common_1 = require("@nestjs/common");
const jwt_1 = require("@nestjs/jwt");
const constants_1 = require("../constants");
let JwtPayloadService = exports.JwtPayloadService = class JwtPayloadService {
    constructor(jwtService) {
        this.jwtService = jwtService;
    }
    generateResetPasswordToken(email) {
        return this.jwtService.sign({ email }, { expiresIn: constants_1.EXPIRES_IN });
    }
    verify(token) {
        try {
            this.jwtService.verify(token);
        }
        catch (e) {
            throw new common_1.BadRequestException('Invalid token');
        }
    }
    decode(token) {
        return this.jwtService.decode(token);
    }
    async createJwtPayload(userTokenDto) {
        const data = { ...userTokenDto };
        try {
            const jwt = this.jwtService.sign(data);
            return {
                expiresIn: constants_1.EXPIRES_IN,
                token: jwt,
                user: { ...userTokenDto },
            };
        }
        catch (error) {
            throw new common_1.HttpException('Internal Server error:' + error.message, common_1.HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
};
exports.JwtPayloadService = JwtPayloadService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [jwt_1.JwtService])
], JwtPayloadService);
//# sourceMappingURL=jwt-payload.service.js.map