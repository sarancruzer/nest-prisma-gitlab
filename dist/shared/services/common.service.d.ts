import { ResponseDto } from '../dto/response.dto';
export declare class CommonService {
    customResponse(data: object, message: string, status: string): ResponseDto;
    customResponseToken(data: object, message: string, status: string): ResponseDto;
    generateUID(): string;
    camelToSnakeCase: (text: string) => string;
    cleanUpObjectKeys(dataObject: object): any;
}
