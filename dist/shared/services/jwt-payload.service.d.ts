import { JwtService } from '@nestjs/jwt';
import { UserTokenDto } from 'src/auth/dto/login-user.dto';
export declare class JwtPayloadService {
    private readonly jwtService;
    constructor(jwtService: JwtService);
    generateResetPasswordToken(email: string): string;
    verify(token: string): void;
    decode(token: string): string | {
        [key: string]: any;
    };
    createJwtPayload(userTokenDto: UserTokenDto | any): Promise<any>;
}
