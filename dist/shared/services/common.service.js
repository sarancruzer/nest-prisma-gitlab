"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CommonService = void 0;
const common_1 = require("@nestjs/common");
const response_dto_1 = require("../dto/response.dto");
const _ = require("lodash");
let CommonService = exports.CommonService = class CommonService {
    constructor() {
        this.camelToSnakeCase = (text) => {
            return text
                .replace(/(.)([A-Z][a-z]+)/, '$1_$2')
                .replace(/([a-z0-9])([A-Z])/, '$1_$2')
                .toLowerCase();
        };
    }
    customResponse(data, message, status) {
        const dto = new response_dto_1.ResponseDto();
        dto.status = status;
        dto.message = message;
        dto.data = data;
        return dto;
    }
    customResponseToken(data, message, status) {
        const dto = new response_dto_1.ResponseDto();
        dto.status = status;
        dto.message = message;
        dto.data = data;
        return dto;
    }
    generateUID() {
        let firstPart = ((Math.random() * 46656) | 0).toString();
        let secondPart = ((Math.random() * 46656) | 0).toString();
        firstPart = ('000' + firstPart).slice(-3);
        secondPart = ('000' + secondPart).slice(-3);
        const uuid = firstPart + secondPart;
        return uuid;
    }
    cleanUpObjectKeys(dataObject) {
        return _.transform(dataObject, function (resultObject, value, key) {
            const newKey = _.snakeCase(_.trim(key));
            resultObject[newKey] = value;
        });
    }
};
exports.CommonService = CommonService = __decorate([
    (0, common_1.Injectable)()
], CommonService);
//# sourceMappingURL=common.service.js.map