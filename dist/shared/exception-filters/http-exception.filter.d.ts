import { ExceptionFilter, ArgumentsHost, HttpException, BadRequestException } from '@nestjs/common';
export declare class HttpExceptionFilter implements ExceptionFilter<HttpException | BadRequestException> {
    catch(exception: HttpException, host: ArgumentsHost): void;
}
