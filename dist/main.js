"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const app_module_1 = require("./app.module");
const config_1 = require("@nestjs/config");
const cookieParser = require("cookie-parser");
const express_1 = require("express");
const swagger_1 = require("@nestjs/swagger");
const path_1 = require("path");
const validation_pipe_1 = require("./shared/pipes/validation.pipe");
const common_1 = require("@nestjs/common");
async function bootstrap() {
    const appOptions = { cors: true };
    const app = await core_1.NestFactory.create(app_module_1.AppModule, appOptions);
    const config = new config_1.ConfigService();
    app.use(cookieParser());
    app.setGlobalPrefix('api');
    app.enableCors();
    app.use((0, express_1.json)({ limit: '50mb' }));
    app.use((0, express_1.urlencoded)({ extended: true, limit: '50mb' }));
    app.useStaticAssets((0, path_1.join)(__dirname, '..', 'public'));
    bootstrapSwagger(app, config);
    app.useGlobalPipes(new validation_pipe_1.ValidationPipe());
    const port = process.env.PORT || 3001;
    await app.listen(port, '0.0.0.0');
    common_1.Logger.log(`Application is running on: ${port} - ${await app.getUrl()}`);
    common_1.Logger.log(`Application is running in : ${await config.get('ENVIRONMENT')} mode`);
}
bootstrap();
const bootstrapSwagger = (app, config) => {
    if (config.get('ENVIRONMENT') === 'development') {
        const configs = new swagger_1.DocumentBuilder()
            .setTitle('UC Api')
            .setVersion('1.0')
            .addBearerAuth({
            type: 'http',
            scheme: 'bearer',
            bearerFormat: 'JWT',
            name: 'JWT',
            description: 'Enter JWT token',
            in: 'header',
        }, 'JWT-auth')
            .build();
        const document = swagger_1.SwaggerModule.createDocument(app, configs);
        swagger_1.SwaggerModule.setup('api', app, document);
    }
};
//# sourceMappingURL=main.js.map