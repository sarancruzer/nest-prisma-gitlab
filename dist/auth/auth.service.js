"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var AuthService_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthService = void 0;
const common_1 = require("@nestjs/common");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const login_user_dto_1 = require("./dto/login-user.dto");
const bcrypt = require("bcrypt");
const jwt_payload_service_1 = require("../shared/services/jwt-payload.service");
const prisma_service_1 = require("../prisma.service");
let AuthService = exports.AuthService = AuthService_1 = class AuthService {
    constructor(prisma, jwtPayloadService) {
        this.prisma = prisma;
        this.jwtPayloadService = jwtPayloadService;
        this.secret = '';
        this.expiresIn = '';
        this.logger = new common_1.Logger(AuthService_1.name);
        this.secret = process.env.JWT_SECRET;
        this.expiresIn = process.env.JWT_EXPIRESIN;
    }
    async register(signupUserDto) {
        this.logger.log('register started');
        const errors = await (0, class_validator_1.validate)(signupUserDto);
        if (errors.length > 0) {
            throw new common_1.HttpException({ errors }, common_1.HttpStatus.BAD_REQUEST);
        }
        const { email, password } = signupUserDto;
        const isExistUser = await this.prisma.user.findUnique({
            where: {
                email: email,
            },
        });
        console.log('🚀 ~ file: auth.service.ts:40 ~ AuthService ~ register ~ isExistUser:', isExistUser);
        if (isExistUser) {
            this.logger.error('User already exists');
            throw new common_1.HttpException('User already exists!', common_1.HttpStatus.BAD_REQUEST);
        }
        signupUserDto.password = bcrypt.hashSync(password, 10);
        console.log('🚀 ~ file: auth.service.ts:49 ~ AuthService ~ register ~ signupUserDto:', signupUserDto);
        const newUser = await this.prisma.user.create({
            data: {
                ...signupUserDto,
            },
        });
        return newUser;
    }
    async authenticate(loginUserDto) {
        this.logger.log('authenticate started');
        const errors = await (0, class_validator_1.validate)(loginUserDto);
        if (errors.length > 0) {
            throw new common_1.HttpException(errors, common_1.HttpStatus.BAD_REQUEST);
        }
        const { email } = loginUserDto;
        const user = await this.prisma.user.findUnique({
            where: {
                email: email,
            },
        });
        const error = { message: loginUserDto.email + ' User not found' };
        if (!user) {
            this.logger.log('User not found');
            throw new common_1.HttpException({ errors: error }, common_1.HttpStatus.UNAUTHORIZED);
        }
        if (bcrypt.compareSync(loginUserDto.password, user.password)) {
            const userData = (0, class_transformer_1.plainToClass)(login_user_dto_1.UserTokenDto, user);
            const tokenData = await this.jwtPayloadService
                .createJwtPayload(userData)
                .then((res) => res);
            return tokenData;
        }
        this.logger.log('Password is wrongg!');
        throw new common_1.HttpException({ errors: { message: 'Password is wrongg!' } }, common_1.HttpStatus.UNAUTHORIZED);
    }
    async findOne(username) {
        return this.prisma.user.findUnique({
            where: {
                email: username,
            },
        });
    }
    async validateUser(username, pass) {
        const user = await this.findOne(username);
        if (user && user.password === pass) {
            const { ...result } = user;
            return result;
        }
        return null;
    }
};
exports.AuthService = AuthService = AuthService_1 = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService,
        jwt_payload_service_1.JwtPayloadService])
], AuthService);
//# sourceMappingURL=auth.service.js.map