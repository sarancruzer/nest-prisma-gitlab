import { SignupUserDto } from './dto/auth-dto';
import { LoginUserDto, TokenDto } from './dto/login-user.dto';
import { JwtPayloadService } from 'src/shared/services/jwt-payload.service';
import { PrismaService } from 'src/prisma.service';
import { User as UserModel } from '@prisma/client';
export declare class AuthService {
    private prisma;
    private jwtPayloadService;
    secret: string;
    expiresIn: string;
    private readonly logger;
    constructor(prisma: PrismaService, jwtPayloadService: JwtPayloadService);
    register(signupUserDto: SignupUserDto): Promise<UserModel>;
    authenticate(loginUserDto: LoginUserDto): Promise<TokenDto>;
    findOne(username: string): Promise<UserModel | undefined>;
    validateUser(username: string, pass: string): Promise<any>;
}
