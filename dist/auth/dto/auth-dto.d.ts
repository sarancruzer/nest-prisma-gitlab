export declare class SignupUserDto {
    name: string;
    email: string;
    mobile: string;
    password: string;
}
declare const UpdateUserDto_base: import("@nestjs/mapped-types").MappedType<Partial<SignupUserDto>>;
export declare class UpdateUserDto extends UpdateUserDto_base {
}
export declare class RequestDto {
    message: string;
    status: string;
    data: object;
}
export declare class ResponseDto {
    message: string;
    status: string;
    data: object;
    token?: string;
}
export {};
