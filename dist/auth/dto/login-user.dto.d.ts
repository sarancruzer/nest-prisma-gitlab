export declare class LoginUserDto {
    email: string;
    password: string;
}
export declare class UserTokenDto {
    id: string;
    name: string;
    email: string;
    mobileNumber: string;
    token: string;
    passwordFlag: number;
    emailVerify: number;
}
export declare class TokenDto {
    expiresIn: number;
    user: UserTokenDto;
    token: string;
}
