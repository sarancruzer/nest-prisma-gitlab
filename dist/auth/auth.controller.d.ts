import { ResponseDto, SignupUserDto } from './dto/auth-dto';
import { LoginUserDto } from './dto/login-user.dto';
import { AuthService } from './auth.service';
export declare class AuthController {
    private readonly authService;
    constructor(authService: AuthService);
    userRegister(signupUserDto: SignupUserDto): Promise<ResponseDto>;
    authenticate(loginUserDto: LoginUserDto): Promise<any>;
    customResponse(data: object, message: string, status: string): Promise<ResponseDto>;
}
