"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserEntity = void 0;
const class_validator_1 = require("class-validator");
const typeorm_1 = require("typeorm");
let UserEntity = exports.UserEntity = class UserEntity {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)('increment', { name: 'id' }),
    __metadata("design:type", Number)
], UserEntity.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', length: 25 }),
    __metadata("design:type", String)
], UserEntity.prototype, "name", void 0);
__decorate([
    (0, class_validator_1.IsEmail)(),
    (0, typeorm_1.Column)({ type: 'varchar', name: 'email', length: 100 }),
    __metadata("design:type", String)
], UserEntity.prototype, "email", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'text', name: 'password' }),
    __metadata("design:type", String)
], UserEntity.prototype, "password", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', name: 'mobile', length: 20 }),
    __metadata("design:type", String)
], UserEntity.prototype, "mobile", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'boolean', name: 'google_auth', default: false }),
    __metadata("design:type", Boolean)
], UserEntity.prototype, "googleAuth", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'boolean', name: 'facebook_auth', default: false }),
    __metadata("design:type", Boolean)
], UserEntity.prototype, "facebookAuth", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'boolean', name: 'password_flag', default: false }),
    __metadata("design:type", Boolean)
], UserEntity.prototype, "passwordFlag", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'boolean', name: 'email_verify', default: false }),
    __metadata("design:type", Boolean)
], UserEntity.prototype, "emailVerify", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'boolean', name: 'mobile_verify', default: false }),
    __metadata("design:type", Boolean)
], UserEntity.prototype, "mobileVerify", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)({ type: 'timestamp', name: 'created_at' }),
    __metadata("design:type", Date)
], UserEntity.prototype, "createdAt", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)({ type: 'timestamp', name: 'updated_at' }),
    __metadata("design:type", Date)
], UserEntity.prototype, "updatedAt", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'integer', name: 'status', default: 1 }),
    __metadata("design:type", Number)
], UserEntity.prototype, "status", void 0);
exports.UserEntity = UserEntity = __decorate([
    (0, typeorm_1.Entity)({ name: 'users' })
], UserEntity);
//# sourceMappingURL=user.entity.js.map