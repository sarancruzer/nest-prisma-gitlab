export declare class UserEntity {
    id: number;
    name: string;
    email: string;
    password: string;
    mobile: string;
    googleAuth: boolean;
    facebookAuth: boolean;
    passwordFlag: boolean;
    emailVerify: boolean;
    mobileVerify: boolean;
    createdAt: Date;
    updatedAt: Date;
    status: number;
}
