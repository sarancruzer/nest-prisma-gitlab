import { UsersService } from './users.service';
import { User, Post as PostModel } from '@prisma/client';
export declare class UsersController {
    private readonly usersService;
    constructor(usersService: UsersService);
    getAllUsers(): Promise<User[]>;
    getAllPosts(): Promise<PostModel[]>;
}
