import { CommonService } from 'src/shared/services/common.service';
import { Post, User } from '@prisma/client';
import { PrismaService } from 'src/prisma.service';
export declare class UsersService {
    private prisma;
    private commonService;
    constructor(prisma: PrismaService, commonService: CommonService);
    getallUsers(): Promise<User[]>;
    getAllPosts(): Promise<Post[]>;
}
