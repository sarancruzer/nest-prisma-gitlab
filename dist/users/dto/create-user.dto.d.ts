export declare class CreateUserDto {
}
export declare class CreateAdminDto {
    name: string;
    email: string;
    mobile: string;
    password: string;
    roleId: number;
    status: number;
}
